-- use servicedesk_orig
-- create table WorkOrderThreads ( workorderid bigint,threaddate bigint,description ntext, processed varchar(5),type varchar(20),notificationid bigint, workorder_created_time bigint, workername varchar(20))
-- alter table workorderthreads add type varchar(20)
-- alter table workorderthreads add workorder_created_time bigint
-- alter table workorderthreads add workername varchar(20)
-- truncate table WorkOrderThreads
-- begin transaction
-- drop table workorderthreads
-- commit
-- select workorderid from workorderthreads order by workorderid
-- select * from workorder_attachments_migrate order by requestid desc
-- delete workorderthreads where type = 'Comment'
-- select count(*) from workorderthreads
-- select * from workorderthreads where workorderid = 11900
--  select * from workorderthreads order by workorderid desc, threaddate asc
-- insert workorderthreads select top 1 987654321,threaddate,description,null,type,notificationid,workorder_created_time from Workorderthreads where processed = 'Y'
-- create table workorder_atach
-- union conversation order by date
-- SELECT top 1000 RequestId, AttachmentId from workorder_attachments_migrate where processed is null order by RequestId desc,attachmentId asc
-- select * from workordersnotopen
-- Workorder Descriptions
-- delete workorderthreads where processed is null
-- select count(*) from workorderthreads where processed is null


------------ Descriptions

insert into WorkOrderThreads
-- select RequestId = 123456789, --987654321,--WorkOrder.WORKORDERID,
select RequestId = WorkOrder.WORKORDERID,
	-- notification.notificationid,
	workorder.CREATEDTIME,
	workordertodescription.fulldescription,
	null	,
	'Comment',
	null,
	workorder.createdtime,
	'worker2'

from WorkOrder
left outer join WorkOrderStates on (WorkOrder.WORKORDERID = WorkOrderStates.WORKORDERID)
left outer join StatusDefinition on (WorkOrderStates.statusid = StatusDefinition.statusid)
left outer join workordertodescription on (workordertodescription.workorderid = workorder.workorderid)
where
-- StatusDefinition.STATUSNAME not like 'Closed%'
--StatusDefinition.STATUSNAME != 'Open'
-- and 
workorder.workorderid in (select top 10000 workorderid from workordersnotopen where processed is null order by workorderid asc)

------------- Notifications

insert into WorkOrderThreads
select RequestId = WorkOrder.WORKORDERID,
	-- notification.notificationid,
	notification.notificationdate,
	notificationtodesc.description,
	null,
	'WorkNote',
	notification.notificationid,
	workorder.createdtime	,
	'worker2'	

from WorkOrder
left outer join WorkOrderStates on (WorkOrder.WORKORDERID = WorkOrderStates.WORKORDERID)
join Notify_WorkOrder on (Notify_workorder.workorderid = workorder.workorderid)
join notification on (notification.notificationid = notify_workorder.notificationid)
join notificationToDesc on (notificationtodesc.notificationid = notification.notificationid)
left outer join StatusDefinition on (WorkOrderStates.statusid = StatusDefinition.statusid)

--where StatusDefinition.STATUSNAME like 'Closed%'
-- where StatusDefinition.STATUSNAME != 'Open'
--and 
where workorder.workorderid in (select top 10000 workorderid from workordersnotopen where processed is null order by workorderid asc)

--------------- Notes

insert into WorkOrderThreads
select RequestId = WorkOrder.WORKORDERID,
	--notes.notesid,
	notes.notesdate,
	notes.notestext,
	null,
	'WorkNote',
	notes.notesid,
	workorder.createdtime,
	'worker2'

from WorkOrder
left outer join WorkOrderStates on (WorkOrder.WORKORDERID = WorkOrderStates.WORKORDERID)
join notes on (notes.workorderid = workorder.workorderid)
left outer join StatusDefinition on (WorkOrderStates.statusid = StatusDefinition.statusid)

--where StatusDefinition.STATUSNAME like 'Closed%'
-- where StatusDefinition.STATUSNAME != 'Open'
--and 
where workorder.workorderid in (select top 10000 workorderid from workordersnotopen where processed is null order by workorderid asc)

--------------- Conversations

insert into WorkOrderThreads
select RequestId = WorkOrder.WORKORDERID,
	--notes.notesid,
	conversation.createdtime,
	conversationdescription.description,
	null,
	'WorkNote',
	conversation.CONVERSATIONID,
	workorder.CREATEDBYID,
	'worker2'
		
from WorkOrder
left outer join WorkOrderStates on (WorkOrder.WORKORDERID = WorkOrderStates.WORKORDERID)
join conversation on (conversation.workorderid = workorder.workorderid)
join conversationDescription on (conversationdescription.conversationid = conversation.conversationid)
left outer join StatusDefinition on (WorkOrderStates.statusid = StatusDefinition.statusid)

--where StatusDefinition.STATUSNAME like 'Closed%'
-- where StatusDefinition.STATUSNAME != 'Open'
-- and 
where workorder.workorderid in (select top 10000 workorderid from workordersnotopen where processed is null order by workorderid asc)



---- ==========================================
-- select * from workorderthreads where type = 'WorkNote'

