

select RequestId = WorkOrder.WORKORDERID,
		Description = isnull(WorkOrder.description,''),
		processed = null

from WorkOrder
left outer join WorkOrderStates on (WorkOrder.WORKORDERID = WorkOrderStates.WORKORDERID)
left outer join StatusDefinition on (WorkOrderStates.STATUSID = StatusDefinition.STATUSID)


where StatusDefinition.STATUSNAME = 'Open'

-- and workorder.workorderid in (select top 10000 workorderid from workordersnotopen where processed is null order by workorderid desc)
