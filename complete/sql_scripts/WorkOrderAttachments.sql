
-- use servicedesk_orig
-- drop table workorder_attachments_migrate
-- create table workorder_attachments_migrate (RequestId bigint, AttachmentId bigint, type varchar(20), processed varchar(5))
-- select * from workorder_attachments_migrate where type = 'WorkOrder' order by RequestId desc
-- update workorder_attachments_migrate set processed = null where processed = 'Y'
-- truncate table workorder_attachments_migrate


-- WorkOrder attachements
-- 

insert workorder_attachments_migrate
select RequestId = WorkOrder.WORKORDERID,
		AttachmentId = SDeskAttachment.attachmentid,
		'WorkOrder',
		processed = null

from WorkOrder
left outer join WorkOrderStates on (WorkOrder.WORKORDERID = WorkOrderStates.WORKORDERID)
join WorkOrderAttachment on (WorkOrderAttachment.workorderid = Workorder.workorderid)
join SDeskAttachment on (Sdeskattachment.attachmentid = workorderattachment.attachmentid)
left outer join StatusDefinition on (WorkOrderStates.statusid = StatusDefinition.statusid)
--where workorder.WORKORDERID=366045
--where StatusDefinition.STATUSNAME not like 'Closed%'
where workorder.WORKORDERID in (select workorderid from WorkOrdersNotOpen where processed = 'Y')

-- and workorder.workorderid = 366025
-- select * from workorder where workorderid = 366070
-- where workorder.workorderid >=  366000 and workorder.workorderid <= 366999
--order by workorder.workorderid desc
-- alter
-- truncate table workorder_attachments_migrate
 -- select * from workorder_attachments_migrate order by requestid desc,attachmentid


-- attachments tied to Notifications

insert workorder_attachments_migrate
select workorder.workorderid,Notify_Attachments.attachmentid,'Notification',null

from WorkOrder
left outer join WorkOrderStates on (WorkOrder.WORKORDERID = WorkOrderStates.WORKORDERID)
join Notify_WorkOrder on (Notify_workorder.workorderid = workorder.workorderid)
join notification on (notification.notificationid = notify_workorder.notificationid)
join notificationToDesc on (notificationtodesc.notificationid = notification.notificationid)

left outer join StatusDefinition on (WorkOrderStates.statusid = StatusDefinition.statusid)
join Notify_Attachments on (Notify_Attachments.NOTIFICATIONID = notification.NOTIFICATIONID)
--where StatusDefinition.STATUSNAME not like 'Closed%'
where workorder.WORKORDERID in (select workorderid from WorkOrdersNotOpen where processed = 'Y')


-- attachments tied to conversations

insert workorder_attachments_migrate
select workorder.workorderid,ConversationAttachment.attachmentid,'Conversation',null

from WorkOrder
left outer join WorkOrderStates on (WorkOrder.WORKORDERID = WorkOrderStates.WORKORDERID)
join conversation on (conversation.workorderid = workorder.workorderid)
join conversationDescription on (conversationdescription.conversationid = conversation.conversationid)
left outer join StatusDefinition on (WorkOrderStates.statusid = StatusDefinition.statusid)
join ConversationAttachment on (ConversationAttachment.CONVERSATIONID = Conversation.CONVERSATIONID)

--where StatusDefinition.STATUSNAME not like 'Closed%'

where workorder.WORKORDERID in (select workorderid from WorkOrdersNotOpen where processed = 'Y')

