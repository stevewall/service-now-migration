package hello;

public class NotificationThread {
    private Long workOrderId;
    private String type;
    private long threadDate;
    private String description;
    private String snSysId;

    public NotificationThread(long workOrderId, String type, long threadDate, String description) {
        this.workOrderId = workOrderId;
        this.type = type;
        this.threadDate = threadDate;
        this.description = description;
    }

    @Override
    public String toString() {
        //return String.format(
        //        "NotificationTHread [workorderId=%d, sn_SysId = '%s', threadDate='%s', description='%s (%d)']",
        //        workOrderId, snSysId, threadDate, description.substring(0,25),description.length());

        return String.format(
                "NotificationTHread [workorderId=%d, type = '%s', sn_SysId = '%s', threadDate='%s', description=(%d)']",
                workOrderId, type, snSysId, threadDate, description.length());
    }

    public Long getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(long workOrderId) {
        this.workOrderId = workOrderId;
    }

    public long getThreadDate() {
        return threadDate;
    }

    public void setThreadDate(long threadDate) {
        this.threadDate = threadDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSnSysId() {
        return snSysId;
    }

    public void setSnSysId(String snSysId) {
        this.snSysId = snSysId;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setWorkOrderId(Long workOrderId) {
        this.workOrderId = workOrderId;
    }


}

