package attachment;

public class AttachmentRecord {
    private Long requestId;
    private String type;
    private long attachmentId;
    private String description;
    private String snSysId;

    public AttachmentRecord(long requestId, long attachmentId) {
        this.requestId = requestId;
        this.type = type;
        this.attachmentId = attachmentId;
        this.description = description;
    }

    @Override
    public String toString() {
        return String.format(
                "AttachmentRecord [workorderId=%d, sn_SysId = '%s', attachmentId='%s']",
                requestId, snSysId, attachmentId);
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(long requestId) {
        this.requestId = requestId;
    }

    public long getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(long attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSnSysId() {
        return snSysId;
    }

    public void setSnSysId(String snSysId) {
        this.snSysId = snSysId;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setWorkOrderId(Long workOrderId) {
        this.requestId = workOrderId;
    }


}

