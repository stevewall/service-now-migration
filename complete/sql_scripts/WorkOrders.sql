

select RequestId = WorkOrder.WORKORDERID,
		-- WorkOrderStates.priorityid,
		CreationTime = WorkOrder.CREATEDTIME,
		-- CreationTime = convert(varchar(30),dateadd(second,WorkOrder.CREATEDTIME /1000 + 8*60*60, '19700101'),120),
		--convert(varchar(100),WorkOrder.createdtime),
		Priority = isnull(PriorityDefinition.priorityname,''),
		-- WorkOrderStates.STATUSID,
		Status = isnull(StatusDefinition.STATUSNAME,''),
		-- workorder.MODEID,
		Mode = isnull(ModeDefinition.modename,''),
		-- Workorderstates.levelid,
		Level = isnull(leveldefinition.levelname,''),
		Technician = isnull(aaauser2.first_name,isnull(aaauser3.first_name,'')),
		Name = isnull(aaauser.first_name,''),
		-- + ' ' + aaauser.last_name,
		ContactNumber = isnull(aaacontactinfo.landline,''),
		Title = '',
		-- workorder.DEPTID,
		Department = isnull(DepartmentDefinition.deptname,''),
		-- workorder.siteid,
		Site = isnull(SDOrganization.name,''),
		-- workorderstates.categoryid,
		Category = isnull(categorydefinition.categoryname,''),
		-- workorderstates.SUBCATEGORYID,
		SubCategory = isnull(subcategorydefinition.name,''),
		Subject = isnull(workorder.title,'')
		--Description = isnull(WorkOrderToDescription.fulldescription,'')
		--Description = isnull(WorkOrder.description),'')
		
		-- OriginatingSystem = 'Service Desk Plus'

from WorkOrder
left outer join WorkOrderStates on (WorkOrder.WORKORDERID = WorkOrderStates.WORKORDERID)
left outer join StatusDefinition on (WorkOrderStates.STATUSID = StatusDefinition.STATUSID)
left outer join PriorityDefinition on (WorkOrderStates.priorityId = PriorityDefinition.priorityid)
left outer join modedefinition on (WorkOrder.MODEID = modedefinition.MODeid)
left outer join LevelDefinition on (workorderstates.levelid = leveldefinition.levelid)
left outer join categorydefinition on (workorderstates.categoryid = categorydefinition.categoryid)
left outer join subcategorydefinition on (workorderstates.subcategoryid = subcategorydefinition.subcategoryid)
left outer join sitedefinition on (workorder.siteid = sitedefinition.siteid)
left outer join SDOrganization on (sdorganization.org_id = sitedefinition.siteid)
left outer join departmentdefinition on (departmentdefinition.deptid = workorder.deptid)
left outer join workordertodescription on (workordertodescription.workorderid = workorder.workorderid)
left outer join aaauser on (aaauser.user_id = workorder.requesterid)
--left outer join aaauser as aaauser2 on (aaauser2.user_id = workorder.createdbyid)
left outer join aaauser as aaauser2 on (aaauser2.user_id = workorderstates.ownerid)
left outer join aaausercontactinfo on (aaausercontactinfo.user_id = aaauser.user_id)
left outer join aaacontactinfo on (aaacontactinfo.contactinfo_id = aaausercontactinfo.contactinfo_id)
	
--where workorder.WORKORDERID=366045
left outer join workorderhistory on (workorderhistory.workorderid = workorder.workorderid and workorderhistory.operationtime = 1443831864353)
left outer join workorderhistorydiff on (workorderhistory.historyid = workorderhistorydiff.historyid and workorderhistorydiff.columnname = 'ownerid')
left outer join aaauser as aaauser3 on (aaauser3.user_id = convert(bigint,convert(varchar(50),workorderhistorydiff.prev_value)))

-- select count(*) from WorkOrder where WorkOrder.CREATEDTIME < 1425168000000
--where StatusDefinition.STATUSNAME like 'Closed%'
-- and workorder.createdtime > = 1388534400

--and workorder.workorderid in (select top 10000 workorderid from workordersnotopen where processed is null order by workorderid desc)
-- order by CreationTime desc
-- Mar 1, 2015 = 1425168000000
where WorkOrder.CREATEDTIME < 1425168000000
