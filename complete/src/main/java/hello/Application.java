package hello;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.http.HttpEntity;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;


import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.util.EntityUtils;
import com.fasterxml.jackson.databind.JsonNode;

import java.nio.charset.CodingErrorAction;
import java.util.regex.*;

import java.io.IOException;
import java.util.HashMap;


@SpringBootApplication
public class Application implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(Application.class);
    public static void main(String args[]) {
        SpringApplication.run(Application.class, args);
    }

  private org.apache.tomcat.jdbc.pool.DataSource dataSource;

    @Autowired
    JdbcTemplate jdbcTemplate;
    JdbcTemplate deleteTemplate;

    HashMap<Long,String> workorderIdToSysId = new HashMap<Long,String>();



    HttpHost _snHost = new HttpHost("slalomdev1.service-now.com", 443, "https");
    //UsernamePasswordCredentials snCred = new UsernamePasswordCredentials("steve.wall@slalom.com", "Allroad99!");

    String snHost = "slalom1.service-now.com";
    //String snHost = "dev13844.service-now.com";
    //String snHost = "slalomdev1.service-now.com";
    HttpHost snHttpHost = new HttpHost(snHost, 443, "https");

    // sd+ key 96147773-2647-4D56-AB79-F6D69425D7E2


    UsernamePasswordCredentials snCred = new UsernamePasswordCredentials("servicenowtesting@slalom.com", "Pa5zum,6]UNW|p(xs" /*"Xk,wkpSi*f"*/);
    //UsernamePasswordCredentials snCred = new UsernamePasswordCredentials("steve.wall", "Allroad99!" /*"Xk,wkpSi*f"*/);

    //UsernamePasswordCredentials snCred = new UsernamePasswordCredentials("admin", "Allroad99!");
    //UsernamePasswordCredentials snCred = new UsernamePasswordCredentials("steve.wall", "Allroad99!");

    //UsernamePasswordCredentials snCred = new UsernamePasswordCredentials("snmigration@slalom.com", "Pa5zum,6]UNW|p(xs");

    CloseableHttpClient snClient;
    HttpClientContext snContext;

    int lastCount = -1;


    @Override
    public void run(String... strings) throws Exception {


        HttpHost target = new HttpHost("support.slalom.com", 443, "https");
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(
                new AuthScope(target.getHostName(), target.getPort()),
                new UsernamePasswordCredentials("steve.wall", "Slalom20154"));
        CloseableHttpClient httpclient = HttpClients.custom()
                .setDefaultCredentialsProvider(credsProvider).build();

        // SN client
        CredentialsProvider snCredsProvider = new BasicCredentialsProvider();
        snCredsProvider.setCredentials(
                new AuthScope(snHttpHost.getHostName(), snHttpHost.getPort()),
                snCred);
        snClient = HttpClients.custom()
                .setDefaultCredentialsProvider(snCredsProvider).build();

        String workerName = System.getProperty("workerName","not-set");
        if (workerName.equals("not-set")) {
            log.error("Set the worker name.");
            return;
        }




        try {

            // Create AuthCache instance
            AuthCache snAuthCache = new BasicAuthCache();
            // Generate BASIC scheme object and add it to the local
            // auth cache
            BasicScheme snBasicAuth = new BasicScheme();
            snAuthCache.put(snHttpHost, snBasicAuth);

            // Add AuthCache to the execution context
            snContext = HttpClientContext.create();
            snContext.setAuthCache(snAuthCache);


            // Create AuthCache instance
            AuthCache authCache = new BasicAuthCache();
            // Generate BASIC scheme object and add it to the local
            // auth cache
            BasicScheme basicAuth = new BasicScheme();
            authCache.put(target, basicAuth);

            // Add AuthCache to the execution context
            HttpClientContext localContext = HttpClientContext.create();
            localContext.setAuthCache(authCache);


            while (lastCount != 0) {
                lastCount = 0;
                log.info("Querying for Notification Thread records.");
                jdbcTemplate.query(
                        "SELECT top 2000 workorderid, type, threaddate, description from WorkOrderThreads where workername = '" + workerName + "' and processed is null order by workorderid asc,threaddate asc",
                        (rs, rowNum) -> new NotificationThread(
                                rs.getLong("workorderid"), rs.getString("type"), rs.getLong("threaddate"), rs.getString("description"))
                ).forEach(workorderthread -> {
                            try {
                                migrateToSN(workorderthread);
                            } catch (Exception e) {
                                log.error("migrateToSN failed: " + e.getLocalizedMessage());
                                System.exit(-99);
                            }
                        }
                );
                log.info("LastCount = " + lastCount);
            }

        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
        } finally {
            httpclient.close();
        }

    }


    private void migrateToSN(NotificationThread workorderthread) throws Exception {
//        try{
//            Thread.sleep(2000);
//        } catch (Exception e) {
//            log.error(e.getLocalizedMessage());
//        }
        lastCount++;

        String snSysId = getSNSysId(workorderthread.getWorkOrderId());
        if (snSysId == null) {
            updateWorkOrderThreadStatus(workorderthread,"E");
            log.error("Couldn't find SN sys_id for " + workorderthread.getWorkOrderId());
        } else {
            workorderthread.setSnSysId(snSysId);
            log.info(workorderthread.toString());
            if (updateSNIncident(workorderthread) == 0) {
                log.info("Updating workorderthread status to 'Y'");
                updateWorkOrderThreadStatus(workorderthread,"Y");
            } else {
                log.info("Updating workorderthread status to 'E'");
                updateWorkOrderThreadStatus(workorderthread,"E");
            }
        }
    }

    private void updateWorkOrderThreadStatus(NotificationThread wot,String status) throws Exception {

        for (int i = 0; i < 5; ++i) {
            try {
                int rows = jdbcTemplate.update(
                        "update workorderthreads set processed = '" + status +
                                "' where workorderid = " + wot.getWorkOrderId() + " and threaddate = " + wot.getThreadDate());
                log.info("Updated " + rows + " rows");
                if (rows > 0) {
                    return;
                }
                log.info("RETRYING updateWorkOrderThradStatus = " + status);
            } catch (Exception e) {
                log.error("Error updating workorderthreads: " + e.getLocalizedMessage());
            }
        }
        throw new Exception("Couldn't update workorderthreads.");
    }

    private int updateSNIncident(NotificationThread workorderthread) {
        // talk to SN client
        // format post w/ work_notes = discription
        // see what happens.
        HttpPut httpPut = new HttpPut("https://" + snHost + "/api/now/table/incident/"  + workorderthread.getSnSysId());

        int rv = 0;

        try {
            //StringEntity se = new StringEntity("{\"work_notes\":\"[code]Update work_notes from code.[/code]\"}");
            String temp = workorderthread.getDescription();
            temp = temp.replace("\\","\\\\").replaceAll("\"", "\\\\\"")
                    .replaceAll("\u00A0","")
                    .replaceAll("\u0092","")
                    .replaceAll("\u0096","")
                    .replaceAll("\\p{Cc}","");
            //.replace("\n","").replace("\r","").replace("\t", "");

            //java.nio.charset.Charset foo = java.nio.charset.Charset.forName("UTF-8");
            //temp = foo.decode(foo.encode(temp)).toString();
            //.encode(temp);
            //temp = EscapeUtils.encodeURIComponent(temp);
            //java.nio.charset.CharsetDecoder decoder = java.nio.charset.Charset.forName("UTF-8").newDecoder();
            //decoder.onMalformedInput(CodingErrorAction.IGNORE);
            //decoder.onUnmappableCharacter(CodingErrorAction.IGNORE);

            //temp = decoder.decode(java.nio.ByteBuffer.wrap(temp.getBytes())).toString();


            // add code for {"work_notes":"[code]Test work_notes 4[/code]","sd_plus_update_time":"1123991200000"}
            String putString = "{\"work_notes\":\"[code]" + temp + "[/code]\"}";
            //log.info("Posting [" +putString);
            StringEntity se = new StringEntity(putString);
            //"{\"work_notes\":\"[code]" + workorderthread.getDescription().substring(0,20) + "[/code]\"}");



            httpPut.setEntity(se);
            httpPut.setHeader("Content-type", "application/json");
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            CloseableHttpResponse response = snClient.execute(snHttpHost, httpPut, snContext);

            int status = response.getStatusLine().getStatusCode();

            if (status != 200) {
                log.error("Got Staus " + status);
                rv = status;
            }
            response.close();


        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
            rv = -1;
            //rv = 888;
            ; // do nothing371396
        }
        return rv;
    }

    private String getSNSysId(Long workOrderId) {
        String rv = null;

        String cacheValue = workorderIdToSysId.get(workOrderId);
        if (cacheValue != null) {
            log.info("SNSysId cache hit on " + workOrderId);
            return cacheValue;
        }

        try {

            HttpGet httpget = new HttpGet("https://" + snHost + "/api/now/table/incident?u_string_4=" + workOrderId);
            //HttpGet httpget = new HttpGet("https://" + snHost + "/api/now/table/incident?number=INC0010002");

            System.out.println("Executing getSNSysId " + httpget.getRequestLine() + " to target " + snHttpHost);
            for (int i = 0; i < 1; i++) {
                CloseableHttpResponse response = snClient.execute(snHttpHost, httpget, snContext);
                try {
                    System.out.println("----------------------------------------");
                    System.out.println(response.getStatusLine());

                    String ss  = new BasicResponseHandler().handleResponse(response);

                    System.out.println(ss);

                    // parse json, get sys_id
//                    ObjectMapper foo = new ObjectMapper();
//                    JsonNode root = foo.readTree(response.getEntity().toString());
                    //String r = response.getEntity().toString();
                    Pattern p = Pattern.compile("\\\"sys_id\\\":\\\"([0-9abcdef]*)\\\"");
                    Matcher m = p.matcher(ss);
                    if (m.find()) {
                        rv = m.group(1);
                        workorderIdToSysId.put(workOrderId,rv);
                    }


                } catch (Exception e) {
                    int yyy = 0;
                } finally {
                    response.close();
                }
            }
        } finally {
            return rv;
        }
    }
}