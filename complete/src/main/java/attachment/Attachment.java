package attachment;

import hello.NotificationThread;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.commons.io.IOUtils;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.*;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@SpringBootApplication
public class Attachment implements CommandLineRunner {


    private static final Logger log = LoggerFactory.getLogger(Attachment.class);
    public static void main(String args[]) {
        SpringApplication.run(Attachment.class, args);
    }

    private org.apache.tomcat.jdbc.pool.DataSource dataSource;
    int lastCount = -1;

    @Autowired
    JdbcTemplate jdbcTemplate;
    JdbcTemplate deleteTemplate;

    HashMap<Long,String> workorderIdToSysId = new HashMap<Long,String>();



    HttpHost _snHost = new HttpHost("slalomdev1.service-now.com", 443, "https");
    //UsernamePasswordCredentials snCred = new UsernamePasswordCredentials("steve.wall@slalom.com", "Allroad99!");
    String snHost = "slalom1.service-now.com";
    //String snHost = "dev13844.service-now.com";
    //String snHost = "slalomdev1.service-now.com";
    HttpHost snHttpHost = new HttpHost(snHost, 443, "https");

    // sd+ key 96147773-2647-4D56-AB79-F6D69425D7E2


    UsernamePasswordCredentials snCred = new UsernamePasswordCredentials("servicenowtesting@slalom.com", "Pa5zum,6]UNW|p(xs" /*"Xk,wkpSi*f"*/);
    //UsernamePasswordCredentials snCred = new UsernamePasswordCredentials("steve.wall", "Allroad99!" /*"Xk,wkpSi*f"*/);

    //UsernamePasswordCredentials snCred = new UsernamePasswordCredentials("admin", "Allroad99!");
    //UsernamePasswordCredentials snCred = new UsernamePasswordCredentials("steve.wall", "Allroad99!");

    //UsernamePasswordCredentials snCred = new UsernamePasswordCredentials("snmigration@slalom.com", "Pa5zum,6]UNW|p(xs");

    CloseableHttpClient snClient;
    HttpClientContext snContext;
    HttpHost sdClient = new HttpHost("support.slalom.com", 443, "https");
    HttpClientContext sdContext = HttpClientContext.create();


    @Override
    public void run(String... strings) throws Exception {


        // SD client
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(
                new AuthScope(sdClient.getHostName(), sdClient.getPort()),
                new UsernamePasswordCredentials("steve.wall", "Slalom20154"));
        CloseableHttpClient httpclient = HttpClients.custom()
                .setRedirectStrategy(new LaxRedirectStrategy())
                .setDefaultCredentialsProvider(credsProvider).build();

        // Create AuthCache instance
        AuthCache snAuthCache = new BasicAuthCache();
        // Generate BASIC scheme object and add it to the local
        // auth cache
        BasicScheme snBasicAuth = new BasicScheme();
        snAuthCache.put(snHttpHost, snBasicAuth);

        // Add AuthCache to the execution context
        snContext = HttpClientContext.create();
        snContext.setAuthCache(snAuthCache);

        CredentialsProvider snCredsProvider = new BasicCredentialsProvider();
        snCredsProvider.setCredentials(
                new AuthScope(snHttpHost.getHostName(), snHttpHost.getPort()),
                snCred);
        snClient = HttpClients.custom()
                .setDefaultCredentialsProvider(snCredsProvider).build();


        try {

            // Create AuthCache instance
            AuthCache authCache = new BasicAuthCache();
            // Generate BASIC scheme object and add it to the local
            // auth cache
            BasicScheme basicAuth = new BasicScheme();
            authCache.put(sdClient, basicAuth);
            org.apache.http.client.CookieStore cookieStore = new BasicCookieStore();

            // Add AuthCache to the execution context
            sdContext.setAuthCache(authCache);
            sdContext.setCookieStore(cookieStore);


            HttpGet loginGet = new HttpGet("https://support.slalom.com");

            loginGet.setHeader("ContentType", "application/x-www-form-urlencode");

            System.out.println("Executing request " + loginGet.getRequestLine() + " to target " + sdClient);
            for (int i = 2; i < 3; i++) {
                CloseableHttpResponse response = httpclient.execute(sdClient, loginGet, sdContext);
                try {
                    System.out.println("----------------------------------------");
                    System.out.println(response.getStatusLine());
                } finally {
                    response.close();
                }
            }

            HttpPost loginPost = new HttpPost("https://support.slalom.com/servlet/j_security_check");
            ArrayList<org.apache.http.NameValuePair> postParamaters;
            postParamaters = new ArrayList<org.apache.http.NameValuePair>();
            postParamaters.add(new BasicNameValuePair("j_username", "steve.wall"));
            postParamaters.add(new BasicNameValuePair("j_password", "Slalom20154"));
            postParamaters.add(new BasicNameValuePair("domain", "LDAP"));
            postParamaters.add(new BasicNameValuePair("DOMAIN_NAME", "2D"));
            postParamaters.add(new BasicNameValuePair("LDAPEnable", "false"));
            postParamaters.add(new BasicNameValuePair("hidden", "Select+a+Domain"));
            postParamaters.add(new BasicNameValuePair("hidden", "For+Domain"));
            postParamaters.add(new BasicNameValuePair("AdEnable", "true"));
            postParamaters.add(new BasicNameValuePair("DomainCount", "0"));
            postParamaters.add(new BasicNameValuePair("LocalAuth", "No"));
            postParamaters.add(new BasicNameValuePair("LocalAuthWithDomain", "2D"));
            postParamaters.add(new BasicNameValuePair("dynamicUserAddition_status", "true"));
            postParamaters.add(new BasicNameValuePair("localAuthEnable", "true"));
            postParamaters.add(new BasicNameValuePair("logonDomainName", "2D"));
            postParamaters.add(new BasicNameValuePair("loginButton", "Login"));

            loginPost.setEntity(new UrlEncodedFormEntity(postParamaters));
            loginPost.addHeader("Referer", "https://servicedesk.example.com");

            System.out.println("Executing request " + loginPost.getRequestLine() + " to target " + sdClient);
            for (int i = 2; i < 3; i++) {
                CloseableHttpResponse _response = httpclient.execute(sdClient, loginPost, sdContext);
                try {
                    System.out.println("----------------------------------------");
                    System.out.println(_response.getStatusLine());
                    // System.out.println(EntityUtils.toString(_response.getEntity()));
                    org.apache.http.Header headers[] = _response.getAllHeaders();
                    for (org.apache.http.Header h : headers) {
                        log.info(h.getName() + ": " + h.getValue());
                    }
                    HttpEntity entity = _response.getEntity();
                } finally {
                    _response.close();
                }
            }



            while (lastCount != 0) {
                lastCount = 0;
                log.info("Querying for Attachment records.");
                jdbcTemplate.query(
                        "SELECT top 1000 RequestId, AttachmentId from workorder_attachments_migrate where type = 'WorkOrder' and processed is null order by RequestId asc,attachmentId asc",
                        (rs, rowNum) -> new AttachmentRecord(
                                rs.getLong("RequestId"), rs.getLong("AttachmentId"))
                ).forEach(attachmentRecord ->
                                migrateToSN(attachmentRecord)
                );
                log.info("LastCount = " + lastCount);
            }
    } catch (Exception e) {
        log.error(e.getLocalizedMessage());
    } finally {
        httpclient.close();
    }
}

    private void migrateToSN(AttachmentRecord attachmentRecord) {
        lastCount++;

        String snSysId = getSNSysId(attachmentRecord.getRequestId());
        if (snSysId == null) {
            log.error("Couldn't find SN sys_id for " + attachmentRecord.getRequestId());
        } else {
            attachmentRecord.setSnSysId(snSysId);
            log.info(attachmentRecord.toString());
            if (sendSDAttachment(attachmentRecord) == 0) {
                log.info("Updating attachment status to 'Y'");
                updateAttachmentInfo(attachmentRecord, "Y");
            } else {
                log.info("Updating attachment status to 'E'");
                updateAttachmentInfo(attachmentRecord, "E");
            }
        }
    }

    private void updateAttachmentInfo(AttachmentRecord attachmentRecord, String status) {

        int rows = jdbcTemplate.update(
                "update workorder_attachments_migrate set processed = '" + status +
                        "' where requestId = " + attachmentRecord.getRequestId() + " and AttachmentId = " + attachmentRecord.getAttachmentId());
        log.info("Updated " + rows + " rows");
    }

    private int sendSDAttachment(AttachmentRecord attachmentRecord) {
        // talk to SN client
        // format post w/ work_notes = discription
        // see what happens.

        int rv = 0;

        try {
        HttpGet attachGet = new HttpGet("https://support.slalom.com/servlet/HdFileDownloadServlet?module=Request&ID=" + attachmentRecord.getAttachmentId() + "&delete=false");

        attachGet.setHeader("ContentType", "application/x-www-form-urlencode");

        System.out.println("Executing request " + attachGet.getRequestLine() + " to target " + sdClient);

        CloseableHttpResponse response = null;

        try {
            response = snClient.execute(sdClient, attachGet, sdContext);
            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());

            //String ss = new BasicResponseHandler().handleResponse(response);

            HttpEntity e = response.getEntity();



            byte[] data2 = IOUtils.toByteArray(e.getContent());

            byte[] edata2 = Base64.encodeBase64(data2,true);

            //log.info("Engity:" + edata2);
            //System.out.println(ss);

            // get file name - attachment;filename*="axaptareport.pdf";
            // get contents
            // convert to base64
            // send to SN rest api.
            String filename = response.getFirstHeader("Content-Disposition").getValue();
            Pattern p = Pattern.compile("^attachment;filename.=\\\"(.*)\\\";" /*.*=\"(.*)\"" /*"\\\"sys_id\\\":\\\"([0-9abcdef]*)\\\""*/);
            Matcher m = p.matcher(filename);
            if (m.find()) {
                filename = m.group(1);
            }

            log.info("Would upload " + filename + " to Ticket Number " + attachmentRecord.getRequestId());
           // byte[] data = Base64.encodeBase64(ss.getBytes(),true);
            HttpPost httpPut = new HttpPost("https://" + snHost + "https://slalom1.service-now.com/ecc_queue.do?JSONv2&sysparm_action=insert");


            //StringEntity se = new StringEntity("{\"work_notes\":\"[code]Update work_notes from code.[/code]\"}");
            StringBuffer zz = new StringBuffer();
            String putString = "{\"agent\":\"AttachmentCreator\"," +
                    "\"topic\":\"AttachmentCreator\"," +
                    "\"name\":\"" + filename + ":application/gif\"," +
                    "\"source\":\"incident:" + attachmentRecord.getSnSysId() + "\"," +
                    "\"payload\":\"";
            zz.append(putString);
            for (byte b : edata2){
                zz.append((char)b);
            }
            zz.append("\"}");

            //log.info("Posting [" +putString);
            StringEntity se = new StringEntity(zz.toString());

            httpPut.setEntity(se);
            httpPut.setHeader("Content-type", "application/json");
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            response = snClient.execute(snHttpHost, httpPut, snContext);

            int status = response.getStatusLine().getStatusCode();

            if (status != 200) {
                log.error("Got Status " + status);
                rv = status;
            }
            response.close();
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
        } finally {
                response.close();
        }
    } catch (Exception e) {
        log.error(e.getLocalizedMessage());
    }
    return rv;
}


    private String getSNSysId(Long workOrderId) {
        String rv = null;

        String cacheValue = workorderIdToSysId.get(workOrderId);
        if (cacheValue != null) {
            log.info("SNSysId cache hit on " + workOrderId);
            return cacheValue;
        }

        try {
            HttpGet httpget = new HttpGet("https://" + snHost + "/api/now/table/incident?u_string_4=" + workOrderId);
            //HttpGet httpget = new HttpGet("https://" + snHost + "/api/now/table/incident?number=INC0010002");

            System.out.println("Executing getSNSysId " + httpget.getRequestLine() + " to target " + snHttpHost);
            for (int i = 0; i < 1; i++) {
                CloseableHttpResponse response = snClient.execute(snHttpHost, httpget, snContext);
                try {
                    System.out.println("----------------------------------------");
                    System.out.println(response.getStatusLine());

                    String ss  = new BasicResponseHandler().handleResponse(response);

                    System.out.println(ss);

                    // parse json, get sys_id
//                    ObjectMapper foo = new ObjectMapper();
//                    JsonNode root = foo.readTree(response.getEntity().toString());
                    //String r = response.getEntity().toString();
                    Pattern p = Pattern.compile("\\\"sys_id\\\":\\\"([0-9abcdef]*)\\\"");
                    Matcher m = p.matcher(ss);
                    if (m.find()) {
                        rv = m.group(1);
                        workorderIdToSysId.put(workOrderId,rv);
                    }


                } catch (Exception e) {
                    int yyy = 0;
                } finally {
                    response.close();
                }
            }
    } catch (Exception e) {
        int yyy = 0;
    }
    finally {
            return rv;
        }
    }
}