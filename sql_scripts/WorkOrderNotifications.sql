-- create table WorkOrderThreads ( workorderid bigint,type varchar(20), threaddate bigint,description ntext, processed varchar(5))
-- alter table workorderthreads add type varchar(20)
-- truncate table WorkOrderThreads

-- union conversation order by date

-- Workorder Descriptions
insert into WorkOrderThreads
select top 100 123456789, --RequestId = WorkOrder.WORKORDERID,
	-- notification.notificationid,
	workorder.CREATEDTIME,
	workorder.description,
	null	,
	'Comment-test'

from WorkOrder
left outer join WorkOrderStates on (WorkOrder.WORKORDERID = WorkOrderStates.WORKORDERID)
left outer join StatusDefinition on (WorkOrderStates.statusid = StatusDefinition.statusid)
where 
StatusDefinition.STATUSNAME = 'Open'
--StatusDefinition.STATUSNAME != 'Open'
--and workorder.workorderid in (select top 10000 workorderid from workordersnotopen where processed is null order by workorderid desc)

-------------


insert into WorkOrderThreads
select RequestId = WorkOrder.WORKORDERID,
	-- notification.notificationid,
	notification.notificationdate,
	notificationtodesc.description,
	null,
	'WorkNote'
		

from WorkOrder
left outer join WorkOrderStates on (WorkOrder.WORKORDERID = WorkOrderStates.WORKORDERID)
join Notify_WorkOrder on (Notify_workorder.workorderid = workorder.workorderid)
join notification on (notification.notificationid = notify_workorder.notificationid)
join notificationToDesc on (notificationtodesc.notificationid = notification.notificationid)

left outer join StatusDefinition on (WorkOrderStates.statusid = StatusDefinition.statusid)



where StatusDefinition.STATUSNAME != 'Open'
and workorder.workorderid in (select top 10000 workorderid from workordersnotopen where processed is null order by workorderid desc)



---------------
insert into WorkOrderThreads
select RequestId = WorkOrder.WORKORDERID,
	--notes.notesid,
	notes.notesdate,
	notes.notestext,
	null,
	'WorkNote'
		

from WorkOrder
left outer join WorkOrderStates on (WorkOrder.WORKORDERID = WorkOrderStates.WORKORDERID)
join notes on (notes.workorderid = workorder.workorderid)

left outer join StatusDefinition on (WorkOrderStates.statusid = StatusDefinition.statusid)



where StatusDefinition.STATUSNAME != 'Open'
and workorder.workorderid in (select top 10000 workorderid from workordersnotopen where processed is null order by workorderid desc)




---------------

insert into WorkOrderThreads
select RequestId = WorkOrder.WORKORDERID,
	--notes.notesid,
	conversation.createdtime,
	conversationdescription.description,
	null,
	'WorkNote'
		

from WorkOrder
left outer join WorkOrderStates on (WorkOrder.WORKORDERID = WorkOrderStates.WORKORDERID)
join conversation on (conversation.workorderid = workorder.workorderid)
join conversationDescription on (conversationdescription.conversationid = conversation.conversationid)
left outer join StatusDefinition on (WorkOrderStates.statusid = StatusDefinition.statusid)



where StatusDefinition.STATUSNAME != 'Open'
and workorder.workorderid in (select top 10000 workorderid from workordersnotopen where processed is null order by workorderid desc)




-- select * from WorkOrderThreads order by threaddate
select workorderid,description, max(datalength(description)) from WorkOrderThreads

select * from workorderthreads order by workorderid desc,threaddate asc

-- update workorderthreads set processed = null where workorderid = 371396

-- select * from workorderthreads where workorderid = 371396

-- SELECT workorderid, threaddate, description from WorkOrderThreads where (processed != 'Y' or processed is null) order by workorderid desc,threaddate asc


-- select count(*) from workorderthreads where processed = 'Y'

-- select count(*) from workorderthreads where processed is null


select datalength(description) from WorkOrderThreads order by datalength(description) desc
